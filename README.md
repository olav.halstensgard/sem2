# Mitt program

1. 
Spillet heter catchMeIfYouCan.
catchMeIfYouCan er et spill for to spillere som spiller på samme tastatur.
 

Spillets regler:

Spilleren som er blå skal forsøke å fange spilleren som er rød. 
Spillerne alternerer fra å være blå og rød med et intervall hvor klokken teller ned fra 5 til 0.
Dersom den blå spilleren går på den røde spilleren så scorer han et poeng. Spillet er første man til 5 poeng.

    Det er gitt visuelle hjelpemidler slik at spillerne lettere skal kunne se når rollene byttes.
    Dette gjøre med en klokke som teller ned til 0.
    I det siste sekundet, altså fra 1 til 0 er det også en strobeeffekt.
    Det endres også tekst hvor tanken er at spillen som starter på en side følger instruksen på den siden.
    Deter også bundet en farget prikk som ikke endres til den enkelte spiller for å lettere holde dem avskilt.

    Spilleren som starter til venstre spiller med tastene:
    

    W = Opp
    S = Ned
    A = Venstre 
    D = Høyre


        Spilleren som starter til høyre spiller med tastene:

    Pil opp = Opp
    Pil ned = Ned
    Pil venstre = Venstre 
    Pil høyre = Høyre

    **Ett trykk gir kun en bevegelse. Det er ikke mulig å holde inne knapper for å bevege seg.

    Det er ikke mulig å gå igjennom de gule veggene eller ut av brettet. 


2. 

Spillet er designet etter model-view-controller prinsippet. Spillet er i hovedsak delt opp i klassene model, view, controller og grid.

I klassene som ligger under model defineres logikken og datastrukturen til modellen. Dette innebærer spillerenes egenskaper og brettets egenskaper.

I klassene som ligger under view defineres hvordan spillet visualiseres ved hjelp av Sving rammeverket. 

I klassen som ligger under controller definers hvilke input modelen skal kunne ta imot fra spillerne.

Spillet funger på et slikt vis at brukerne bruker controlleren til endre på modellen mens view tegner modelen basert på statusen til modellen. 

Grid klassen representerer en to dimensjonal liste med den egenskapen at den kan holde på en char verdi. Det er verdiene i gridet som bestemmes hva som skal tegnes på brettet. Sammspillet mellom grid og CellPositionToPixelConverter klassen bestemmer hvor og hvordan en spiller brikke skal
tegnes.

3. Musikkfilene som ligger ved brukes ikke p.d.d men skal brukes til fremtidig funksjonalitet. Men om du har sansen for Synth så er det verdt å sjekke ut White Bat Audio på YouTube.

4. Manuelle tester:

    visualTimerAndChangingOfFightRun()
    En viktig metode i spillet er visualTimer() i CatchMeModel klassen. Denne skal alltid telle fra 5 til 0 for deretter starte på nytt. Denne klokken bestemmer når spillerne endrer verdi, slik at blå blir rød og motsatt, samtidig bytter Run! og Fight! plass. Når visualTimer() er mellom 1 og 0 får man strobe effekt. 

    winCondition()
    For å teste at winCondition() fungeret trenger man bare gå må motstanderen med den blå brikken. Dette vil også oppdatere getScorePlayer1() og getScorePlayer2(). Når en spill oppnår 5 poeng vil spillet endre gamestate til GameOver. Da må spillet startes pånytt.  

5. Videolink: https://youtu.be/QZsUYBSlFCY