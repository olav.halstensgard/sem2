package no.uib.inf101.sem2.catchMeIfYouCan.model.playerUnit;

//Denne klassen tilsvarer Tetromino factory
public interface CreatePlayer {

    /**
     * Lager en ny PlayerUnit.
     * 
     * @return PlayerUnit
     */

    PlayerUnit createPlayers();

}
