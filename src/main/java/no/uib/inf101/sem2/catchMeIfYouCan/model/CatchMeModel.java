package no.uib.inf101.sem2.catchMeIfYouCan.model;

import no.uib.inf101.sem2.catchMeIfYouCan.CatchMeController.ControllableCatchMeModel;
import no.uib.inf101.sem2.catchMeIfYouCan.model.playerUnit.CreatePlayer;
import no.uib.inf101.sem2.catchMeIfYouCan.model.playerUnit.PlayerUnit;
import no.uib.inf101.sem2.catchMeIfYouCan.view.ViewableCatchMeModel;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimensions;

//Denne klassen tilsvarer TetrisModel 
public class CatchMeModel implements ViewableCatchMeModel, ControllableCatchMeModel {

    CatchMeBoard catchMeBoard;

    CreatePlayer createPlayer1;
    CreatePlayer createPlayer2;

    PlayerUnit movingCatchMeModel1;
    PlayerUnit movingCatchMeModel2;

    GameState gameState;

    int winCounterPlayer1;
    int winCounterPlayer2;

    int visualCounter;

    boolean action; // run or fight, bruker boolsk flagg
            /**
     * Lager en en CatchMeModel som består av brettet og to spillere.
     * 
     * @param CatchMeBoard
     * @param CreatePlayer
     * @param Createpalyer
     */
    public CatchMeModel(CatchMeBoard catchMeBoard, CreatePlayer createPlayer1, CreatePlayer createPlayer2) {

        this.catchMeBoard = catchMeBoard;

        this.createPlayer1 = createPlayer1;
        this.createPlayer2 = createPlayer2;

        this.movingCatchMeModel1 = createPlayer1.createPlayers().playerSpawn(1, catchMeBoard);
        this.movingCatchMeModel2 = createPlayer2.createPlayers().playerSpawn(2, catchMeBoard);

        this.winCounterPlayer1 = 0;
        this.winCounterPlayer2 = 0;

        this.visualCounter = 5;

        this.gameState = GameState.ACTIVE_GAME;

        this.action = true;

    }

    @Override
    public GridDimensions getDimension() {
        return this.catchMeBoard;
    }

    @Override
    public Iterable<GridCell<Character>> getTilesOnBoard() {
        return this.catchMeBoard;
    }

    @Override
    public Iterable<GridCell<Character>> movingShape1() {
        
        return this.movingCatchMeModel1;

    }

    @Override
    public Iterable<GridCell<Character>> movingShape2() {

        return this.movingCatchMeModel2;
    }

    @Override
    public boolean moveCatchMeModel(int playerNumber, int deltaRow, int deltaCol) {
        PlayerUnit playerUnit = null;
        if (playerNumber == 1) {
            playerUnit = movingCatchMeModel1;
        } else if (playerNumber == 2) {
            playerUnit = movingCatchMeModel2;
        } else {
            throw new IllegalArgumentException("Invalid player number: " + playerNumber);
        }
        PlayerUnit checkIfLegalCatchMeModel = playerUnit.nextMove(deltaRow, deltaCol);

        if (legalPlayerUnit(checkIfLegalCatchMeModel)) {
            if (playerNumber == 1) {
                movingCatchMeModel1 = checkIfLegalCatchMeModel;
            } else if (playerNumber == 2) {
                movingCatchMeModel2 = checkIfLegalCatchMeModel;
            }

            winCondition();
            return true;
        }
        return false;
    }

    public boolean changeCh(int playerNumber1, int playerNumber2) {

        PlayerUnit playerUnit = null;
        boolean changeCh = true;

        if (playerNumber1 == 1 && playerNumber2 == 2) {

            playerUnit = movingCatchMeModel1;
            movingCatchMeModel1 = playerUnit.nextChar();
            playerUnit = movingCatchMeModel2;
            movingCatchMeModel2 = playerUnit.nextChar();

        } else {
            changeCh = false;
        }
        return changeCh;

    }

    @Override
    public int visualTimerAndChangingOfFightRun() {

        int newTime = this.visualCounter - 1;
        this.visualCounter = newTime;

        if (this.visualCounter <= -1) {
            visualCounter = 5;
            changeCh(1, 2);
            if (this.action == true) {
                action = false;
            } else if (this.action == false) {
                action = true;
            }
        }

        return visualCounter;
    }

    @Override
    public int timerForVisualTimer() {
        return 1000;
    }

    // Tatt fra tetris
    private boolean legalPlayerUnit(PlayerUnit playerUnit) {
        for (GridCell<Character> gridCell : playerUnit) {
            if (!catchMeBoard.positionIsOnGrid(gridCell.pos())) {
                return false;
            }
            if (catchMeBoard.get(gridCell.pos()) != '-') {
                return false;
            }
        }
        return true;
    }

    // winCondition er lagd slik at det er kun spilleren som holder verdien 'a' som
    // kan score.
    private void winCondition() {

        for (GridCell<Character> cell1 : movingCatchMeModel1) {

            for (GridCell<Character> cell2 : movingCatchMeModel2) {

                if (cell1.pos().equals(cell2.pos())) {
                    if (cell1.value() == 'a' && cell2.value() == 'b') {

                        int newScore1 = this.winCounterPlayer1 + 1;

                        this.winCounterPlayer1 = newScore1;
                        this.movingCatchMeModel1 = movingCatchMeModel1.playerSpawn(1, catchMeBoard);
                        this.movingCatchMeModel2 = movingCatchMeModel2.playerSpawn(2, catchMeBoard);

                        if (newScore1 == 5) {

                            gameState = GameState.GAME_OVER;
                        }

                    }

                    else {

                        int newScore2 = this.winCounterPlayer2 + 1;

                        this.winCounterPlayer2 = newScore2;
                        this.movingCatchMeModel1 = movingCatchMeModel1.playerSpawn(1, catchMeBoard);
                        this.movingCatchMeModel2 = movingCatchMeModel2.playerSpawn(2, catchMeBoard);

                        if (newScore2 == 5) {

                            gameState = GameState.GAME_OVER;
                        }
                    }

                }

            }
        }

    }

    @Override
    public GameState getGameState() {
        return gameState;
    }

    @Override
    public int getScorePlayer1() {

        return this.winCounterPlayer1;

    }

    @Override
    public int getScorePlayer2() {

        return this.winCounterPlayer2;

    }

    @Override
    public int timeLeft() {

        return this.visualCounter;

    }

    @Override
    public String fightRunPlayer1() {
        if (this.action == true) {
            return "Fight!";
        } else
            return "Run!";
    }

    @Override
    public String fightRunPlayer2() {
        if (this.action == true) {
            return "Run!";
        } else
            return "Fight!";
    }

}
