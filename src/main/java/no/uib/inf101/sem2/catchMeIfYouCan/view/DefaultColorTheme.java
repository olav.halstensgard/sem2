package no.uib.inf101.sem2.catchMeIfYouCan.view;

import java.awt.Color;

public class DefaultColorTheme implements ColorTheme {

    @Override
    public Color getCellColor(Character c) {

        Color color = switch (c) {
            case 'a' -> Color.BLUE;
            case 'b' -> Color.RED;
            case 'A' -> Color.BLUE;
            case 'B' -> Color.RED;

            case '-' -> Color.BLACK;
            case 'G' -> Color.GREEN;
            case 'Z' -> Color.YELLOW;
            default -> throw new IllegalArgumentException(
                    "No available color for '" + c + "'");
        };
        return color;
    }

    @Override
    public Color getFrameColor() {

        return new Color(0, 0, 0, 0);
    }

    @Override
    public Color getBackgroundColor() {

        return new Color(245, 245, 220);
    }

}
