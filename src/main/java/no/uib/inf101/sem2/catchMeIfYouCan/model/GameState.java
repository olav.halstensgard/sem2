package no.uib.inf101.sem2.catchMeIfYouCan.model;

public enum GameState {

    ACTIVE_GAME,
    GAME_OVER,

}
