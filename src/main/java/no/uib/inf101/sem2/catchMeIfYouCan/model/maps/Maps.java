package no.uib.inf101.sem2.catchMeIfYouCan.model.maps;

import no.uib.inf101.sem2.catchMeIfYouCan.model.CatchMeBoard;
import no.uib.inf101.sem2.grid.CellPosition;

public class Maps implements IMaps {

    CatchMeBoard board;
        /**
     * Henter ferdig lagde kart. Man kan velge mellom map1() og map2()
     */
    public Maps() {

    }

    @Override
    public CatchMeBoard map1() {

        this.board = new CatchMeBoard(15, 15);

        board.set(new CellPosition(1, 1), 'Z');
        board.set(new CellPosition(1, 13), 'Z');
        board.set(new CellPosition(13, 1), 'Z');
        board.set(new CellPosition(13, 13), 'Z');

        board.set(new CellPosition(2, 2), 'Z');
        board.set(new CellPosition(2, 12), 'Z');
        board.set(new CellPosition(12, 2), 'Z');
        board.set(new CellPosition(12, 12), 'Z');

        board.set(new CellPosition(6, 6), 'Z');
        board.set(new CellPosition(6, 7), 'Z');
        board.set(new CellPosition(6, 8), 'Z');
        board.set(new CellPosition(7, 6), 'Z');
        board.set(new CellPosition(7, 7), 'Z');
        board.set(new CellPosition(7, 8), 'Z');
        board.set(new CellPosition(8, 6), 'Z');
        board.set(new CellPosition(8, 7), 'Z');
        board.set(new CellPosition(8, 8), 'Z');

        board.set(new CellPosition(4, 4), 'Z');
        board.set(new CellPosition(4, 5), 'Z');
        board.set(new CellPosition(4, 5), 'Z');
        board.set(new CellPosition(4, 6), 'Z');
        // board.set(new CellPosition(4, 7),'Z');
        board.set(new CellPosition(4, 8), 'Z');
        board.set(new CellPosition(4, 9), 'Z');
        board.set(new CellPosition(4, 10), 'Z');

        board.set(new CellPosition(5, 4), 'Z');
        board.set(new CellPosition(6, 4), 'Z');
        board.set(new CellPosition(8, 4), 'Z');
        board.set(new CellPosition(9, 4), 'Z');
        board.set(new CellPosition(10, 4), 'Z');
        board.set(new CellPosition(10, 5), 'Z');
        board.set(new CellPosition(10, 6), 'Z');
        // board.set(new CellPosition(10, 7),'Z');
        board.set(new CellPosition(10, 8), 'Z');
        board.set(new CellPosition(10, 9), 'Z');
        board.set(new CellPosition(10, 10), 'Z');
        board.set(new CellPosition(9, 10), 'Z');
        board.set(new CellPosition(8, 10), 'Z');
        board.set(new CellPosition(6, 10), 'Z');
        board.set(new CellPosition(5, 10), 'Z');

        return board;

    }

    @Override
    public CatchMeBoard map2() {

        this.board = new CatchMeBoard(10, 10);

        board.set(new CellPosition(1, 1), 'Z');
        board.set(new CellPosition(1, 8), 'Z');
        board.set(new CellPosition(8, 1), 'Z');
        board.set(new CellPosition(8, 8), 'Z');

        board.set(new CellPosition(2, 2), 'Z');
        board.set(new CellPosition(2, 7), 'Z');
        board.set(new CellPosition(7, 2), 'Z');
        board.set(new CellPosition(7, 7), 'Z');

        board.set(new CellPosition(4, 4), 'Z');
        board.set(new CellPosition(4, 5), 'Z');
        board.set(new CellPosition(5, 4), 'Z');
        board.set(new CellPosition(5, 5), 'Z');

        return board;

    }

}
