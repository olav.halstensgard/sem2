package no.uib.inf101.sem2.catchMeIfYouCan.model.playerUnit;

import java.util.ArrayList;
import java.util.Iterator;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimensions;

//Denne klassen tilsvare Tetromino klassen
// I PlayerUnit klassen defineres det hvordan en Player Unit skal se ut og hvilke kordinater den skal tegnes på.
public final class PlayerUnit implements Iterable<GridCell<Character>> {

    char ch;
    boolean[][] shape; // Boolean[][] representerer en to dimensjonal liste som inneholder boolske
                       // verdier.
    CellPosition CP;

    private PlayerUnit(char ch, boolean[][] shape, CellPosition CP) {

        this.ch = ch;
        this.shape = shape;
        this.CP = CP;
 
    }
    /**
     * Lager en ny player unit og bestemmer hvilke fasong.
     * 
     * @param int ch
     * @return PlayerUnit
     */
    public static PlayerUnit newPlayerUnit(char ch) {

        if (ch == 'a') {
            boolean[][] shape = new boolean[][] {
                    { false, false, false },
                    { false, true, false },
                    { false, false, false } };
            return new PlayerUnit(ch, shape, new CellPosition(0, 0));

        }
        if (ch == 'b') {
            boolean[][] shape = new boolean[][] {
                    { false, false, false },
                    { false, true, false },
                    { false, false, false } };
            return new PlayerUnit(ch, shape, new CellPosition(0, 0));

        }
        // Mulig jeg vil lage en sabotasje funksjon, til fremtidig kode
        // if (ch == 'A') {
        // boolean[][] shape = new boolean[][] {
        // { false, false, false, false },
        // { false, true, true, false },
        // { false, true, true, false },
        // { false, false, false, false } };
        // return new PlayerUnit(ch, shape, new CellPosition(0, 0));
        // }
        // if (ch == 'B') {
        // boolean[][] shape = new boolean[][] {
        // { false, false, false, false },
        // { false, true, true, false },
        // { false, true, true, false },
        // { false, false, false, false } };
        // return new PlayerUnit(ch, shape, new CellPosition(0, 0));
        // }
        throw new IllegalArgumentException("not existing shape");
    }

    /**
     * Oppdaterer kordinatene til den aktuelle spilleren.
     * 
     * @param int deltaRow
     * @param int deltaCol
     * @return PlayerUnit
     */

    public PlayerUnit nextMove(int deltaRow, int deltaCol) { // tilsvarer shiftedBy

        int newRowPosition = this.CP.row() + deltaRow;
        int newColPosition = this.CP.col() + deltaCol;

        CellPosition shiftedCellPosition = new CellPosition(newRowPosition, newColPosition);

        return new PlayerUnit(this.ch, this.shape, shiftedCellPosition);

    }

    /**
     * Erstatter eksisterende player units og oppdaterer verdien på PlayerUnit.
     * Dette brukes til endre verdien fra a til b og b til a.
     * 
     * @return PlayerUnit
     */
    public PlayerUnit nextChar() {

        if (this.ch == 'a') {
            char nextChar = 'b';
            return new PlayerUnit(nextChar, this.shape, this.CP);
        }

        else if (this.ch == 'b') {
            char nextChar = 'a';
            return new PlayerUnit(nextChar, this.shape, this.CP);
        }

        else
            throw new IllegalArgumentException("Invalid conversion");

    }

    /**
     * Start/respawn posisojon for spiller 1 og 2
     * 
     * @param GridDimensions
     * @return PlayerUnit
     */
    public PlayerUnit playerSpawn(int player, GridDimensions gridDimension) {

        if (player == 1) {

            int midColumn = ((gridDimension.cols()) - this.shape.length) / 2;
            int startingSide = gridDimension.rows() - 3;
            return new PlayerUnit(this.ch, this.shape, new CellPosition(midColumn, startingSide));
        }

        else if (player == 2) {
            int midColumn = ((gridDimension.cols()) - this.shape.length) / 2;
            int startingSide = gridDimension.rows() - gridDimension.rows();
            return new PlayerUnit(this.ch, this.shape, new CellPosition(midColumn, startingSide));
        }

        else
            throw new IllegalArgumentException("None existing player");
    }

    /**
     * Sjekker om shape, innheolder en true value, henter ut det kordinatet til true
     * verdiene og legger dette til CP (CellPosition)
     * for deretter genere en ny GridCell<Character> med de nye kordinatene. Utgjør
     * kordinatene til spillerne og verdiene.
     * 
     * @return Iterator<GridCell<Character>>
     */
    @Override
    public Iterator<GridCell<Character>> iterator() {

        ArrayList<GridCell<Character>> cellList = new ArrayList<>();
        for (int row = 0; row < shape.length; row++) {

            for (int col = 0; col < shape[0].length; col++) {
                if (shape[row][col]) {
                    int newCol = CP.col() + col;
                    int newRow = CP.row() + row;
                    CellPosition CP = new CellPosition(newRow, newCol);

                    cellList.add(new GridCell<Character>(CP, ch));

                }
            }
        }

        return cellList.iterator();
    }

}
