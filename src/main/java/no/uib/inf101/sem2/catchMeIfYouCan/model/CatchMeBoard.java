package no.uib.inf101.sem2.catchMeIfYouCan.model;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.Grid;

// Ved å extende så blir CatchMeBoard en subklasse av Grid og vil derfor arve egenskapene til Grid.
public class CatchMeBoard extends Grid<Character> {

    public CatchMeBoard(int numberOfRows, int numberOfCollumns) {
        super(numberOfRows, numberOfCollumns, '-');

    }

    /**
     * Setter inn en string verdi på gitt CellPosition.
     * En metode som kun er ment for å teste brettet (og er tatt fra tetris
     * oppgaven)
     * 
     * @return String
     */

    public String prettyString() {

        String s = "";

        for (int r = 0; r < rows(); r++) {
            for (int c = 0; c < cols(); c++) {
                Character character = get(new CellPosition(r, c));
                s += character;

            }
            if (r != rows() - 1)
                s += '\n';
        }

        return s;

    }

}
