package no.uib.inf101.sem2.catchMeIfYouCan.CatchMeController;

import no.uib.inf101.sem2.catchMeIfYouCan.model.GameState;

public interface ControllableCatchMeModel {

     /**
      * Avklarer om det er spiller 1 eller spiller 2 som flyttes. For deretter å
      * kalle på metoden nextMove()
      * 
      * @param int playerNumber
      * @param int deltaRow
      * @param int deltaCol
      * @return boolean
      */
     boolean moveCatchMeModel(int playerNumber, int deltaRow, int deltaCol);

     /** Gamstate er enten ACTIVE_GAME eller GAME_OVER */
     GameState getGameState();

     
     /**
      * Endrer spillerens verdi fra 'a' til 'b' og motsatt. 
      * @param int deltaRow
      * @param int deltaCol
      * @return boolean
      */
     boolean changeCh(int playerNumber1, int playerNumber2);
     /**Klokken timmer ned med 1000 ms som utgangspunkt */
     int timerForVisualTimer();
     /**Kaller på klokka som tikker ned og endrer bolskt flagg for endring av FightRun  */
     int visualTimerAndChangingOfFightRun();
     

}
