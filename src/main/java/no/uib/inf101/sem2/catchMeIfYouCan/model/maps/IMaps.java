package no.uib.inf101.sem2.catchMeIfYouCan.model.maps;

import no.uib.inf101.sem2.catchMeIfYouCan.model.CatchMeBoard;

public interface IMaps {
        /**
     * Lager en ny lager kart 1 som er 15 x 15 stort med en mellom stor labyrint.
     * 
     * @return CatchMeBoard
     */
    CatchMeBoard map1();
        /**
     * Lager en ny lager kart 2 som er 10 x 10 stort med en liten labyrint.
     * 
     * @return CatchMeBoard
     */
    CatchMeBoard map2();

}
