package no.uib.inf101.sem2.catchMeIfYouCan.view;

import javax.swing.JPanel;

import no.uib.inf101.sem2.catchMeIfYouCan.model.GameState;
import no.uib.inf101.sem2.grid.GridCell;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Color;

import java.awt.geom.Ellipse2D;

public class CatchMeView extends JPanel {

    ColorTheme colorTheme;
    ViewableCatchMeModel viewableCatchMeModel;
    Graphics2D graphics2d;
    CellPositionToPixelConverter cellPositionToPixelConverter;
    private static final double OUTERMARGIN = 100;
    private static final double INNERMARGIN = 2;
    
    Color marginColor;
    int gridColor;

    /**
     * Generer et synlig catchMe brett med aktive spillere:
     * 
     * @param ViewableCatchMeModel
     */
    public CatchMeView(ViewableCatchMeModel viewableCatchMeModel) {
        this.colorTheme = new DefaultColorTheme();
        this.viewableCatchMeModel = viewableCatchMeModel;

        this.setBackground(colorTheme.getBackgroundColor());
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(700, 700));

        this.gridColor = 0;

    }

    @Override 
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        int scoreFontSize = 45;
        Font scoreFont = new Font("Papyrus", Font.BOLD, scoreFontSize);
        g2.setFont(scoreFont);

        g.drawString("" + viewableCatchMeModel.getScorePlayer1(), this.getWidth() - this.getWidth() / 10,
                this.getHeight() / 2);
        g.drawString("" + viewableCatchMeModel.getScorePlayer2(), this.getWidth() / 15, this.getHeight() / 2);

        int timeFontSize = 60;
        Font timeFont = new Font("Papyrus", Font.BOLD, timeFontSize);
        g2.setFont(timeFont);

        g.drawString("" + viewableCatchMeModel.timeLeft(), (this.getWidth() / 2) - 20, this.getHeight() / 15);

        g.drawString("" + viewableCatchMeModel.fightRunPlayer2(), this.getWidth() / 10, this.getHeight() / 12);
        g.drawString("" + viewableCatchMeModel.fightRunPlayer2(), this.getWidth() / 10,
                this.getHeight() - this.getHeight() / 20);

        g.drawString("" + viewableCatchMeModel.fightRunPlayer1(), this.getWidth() - (this.getWidth() / 3),
                this.getHeight() / 12);
        g.drawString("" + viewableCatchMeModel.fightRunPlayer1(), this.getWidth() - (this.getWidth() / 3),
                this.getHeight() - this.getHeight() / 20);

        drawGame(g2);

    }

    // Her tegner jeg spiller firkanten
    private void drawGame(Graphics2D square) {

        Rectangle2D canvas = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, this.getWidth() - (2 * OUTERMARGIN),
                this.getHeight() - (2 * OUTERMARGIN));
        // Lager strobe effekt og det er kult. Tegner også på keystrokes!
        if (viewableCatchMeModel.timeLeft() == 5) {
            this.marginColor = colorForGrid();
        }
        square.setColor(marginColor);
        square.fill(canvas);
        CellPositionToPixelConverter cp = new CellPositionToPixelConverter(canvas, viewableCatchMeModel.getDimension(),
                INNERMARGIN);
        drawCells(square, viewableCatchMeModel.getTilesOnBoard(), cp, colorTheme);

        drawPlayer1(square, viewableCatchMeModel.movingShape2(), cp, colorTheme);
        drawPlayer2(square, viewableCatchMeModel.movingShape1(), cp, colorTheme);
        // Visuelt ulogisk, rent kodemessig er spiller 1 til høyre, men det påvirker
        // ikke spillet.
        if (viewableCatchMeModel.getGameState() == GameState.GAME_OVER) {
            square.setColor(Color.RED);
            square.setFont(new Font("Papyrus", Font.BOLD, 30));

            if (this.viewableCatchMeModel.getScorePlayer1() == 5)

            {
                square.drawString("Player 2 Won!", (this.getWidth() / 2) - 100, (this.getHeight() / 2) + 10);

            }
            if (this.viewableCatchMeModel.getScorePlayer2() == 5)

            {
                square.drawString("Player 1 Won!", (this.getWidth() / 2) - 100, (this.getHeight() / 2) + 10);

            }

            square.setColor(new Color(0, 0, 0, 128));
            square.fill(canvas);
        }
    }

    private static void drawCells(Graphics2D canvas, Iterable<GridCell<Character>> iterableGridCellCharacter,
            CellPositionToPixelConverter positionSquares, ColorTheme colorTheme) {

        // her tegnes en og en rute
        for (GridCell<Character> movingShape : iterableGridCellCharacter) {
            Rectangle2D rectangle = positionSquares.getBoundsForCell(movingShape.pos());
            Color col = colorTheme.getCellColor(movingShape.value());
            canvas.setColor(col);
            canvas.fill(rectangle);

        }

    }

    private static void drawPlayer1(Graphics2D canvas, Iterable<GridCell<Character>> iterableGridCellCharacter,
            CellPositionToPixelConverter positionSquares, ColorTheme colorTheme) {

        for (GridCell<Character> movingShape : iterableGridCellCharacter) {
            Rectangle2D rectangle = positionSquares.getBoundsForCell(movingShape.pos());
            Color col = colorTheme.getCellColor(movingShape.value()); // finne karakter objektet
            canvas.setColor(col);
            canvas.fill(rectangle);

            // Hjelper meg å forstå helheten av iterableGridCellCharacter og metodene som
            // kan anvendes for å hente informasjon
            // System.out.println(iterableGridCellCharacter.iterator().next().pos());

            {
                // Lage svart prikk på spiller to.
                double centerX = rectangle.getCenterX();

                double centerY = rectangle.getCenterY();

                double radius = Math.min(rectangle.getWidth(), rectangle.getHeight()) / 6.0;
                Ellipse2D dot = new Ellipse2D.Double(centerX - radius, centerY - radius, radius * 2, radius * 2);
                // canvas.setColor(new Color(0, 200, 0));
                canvas.setColor(Color.BLACK);
                canvas.fill(dot);
            }

        }

    }

    private static void drawPlayer2(Graphics2D canvas, Iterable<GridCell<Character>> iterableGridCellCharacter,
            CellPositionToPixelConverter positionSquares, ColorTheme colorTheme) {
        for (GridCell<Character> movingShape : iterableGridCellCharacter) {
            Rectangle2D rectangle = positionSquares.getBoundsForCell(movingShape.pos());
            Color col = colorTheme.getCellColor(movingShape.value()); // finne karakter objektet
            canvas.setColor(col);
            canvas.fill(rectangle);
            {
                // Lage prikk på spiller en.
                double centerX = rectangle.getCenterX();

                double centerY = rectangle.getCenterY();

                double radius = Math.min(rectangle.getWidth(), rectangle.getHeight()) / 6.0;
                Ellipse2D dot = new Ellipse2D.Double(centerX - radius, centerY - radius, radius * 2, radius * 2);
                // canvas.setColor(new Color(200, 200, 0));
                canvas.setColor(Color.WHITE);
                canvas.fill(dot);
            }

        }

    }

    // Farger til gridet og strobe effekt
    private Color colorForGrid() {
        ArrayList<Color> colorList = new ArrayList<>();
        Color greenish = new Color(0, 180, 0);
        Color redish = new Color(180, 0, 0);
        Color blueish = new Color(0, 0, 180);
        colorList.add(greenish);
        colorList.add(redish);
        colorList.add(blueish);

        gridColor++;

        if (gridColor == 3) {
            gridColor = 0;
        }

        Color color = colorList.get(gridColor);

        return color;
    }

}
