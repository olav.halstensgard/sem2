package no.uib.inf101.sem2.catchMeIfYouCan.view;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimensions;

public class CellPositionToPixelConverter {

  Rectangle2D box;
  GridDimensions gd;
  double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimensions gd, double margin) {

    this.box = box;
    this.gd = gd;
    this.margin = margin;

  }

  /**
   * Oversetter kordinater i rutenetet til et rektangel med posisjon og størrelse
   * på brettet.
   * 
   * @return Rectangle2D
   */
  // Denne klassen fikk jeg help med når vi lagde tetris og har i storgrad blitt
  // gjennbrukt.
  public Rectangle2D getBoundsForCell(CellPosition cellPosition) { // Spillet innenfor rammen

    int widthmargins = this.gd.cols() + 1;
    // bredde - 2* Outermagin

    double cellWidth = (this.box.getWidth() - widthmargins * this.margin) / this.gd.cols(); // Dette er antall marginer
                                                                                            // i bredden

    int heightMargins = this.gd.rows() + 1;
    double cellHeight = (this.box.getHeight() - heightMargins * this.margin) / this.gd.rows(); // Dette er antall
                                                                                               // marginer i høyden

    // Det er her "magien" skjer, er brukes verdien til kordinatene til å beskrive
    // hvor det skal tegnes ruter.
    double cellX = box.getX() + this.margin * (cellPosition.col() + 1) + cellWidth * cellPosition.col();

    double cellY = box.getY() + this.margin * (cellPosition.row() + 1) + cellHeight * cellPosition.row();

    Rectangle2D.Double cellBounds = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
    return cellBounds;

  }

}
