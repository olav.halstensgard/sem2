package no.uib.inf101.sem2.catchMeIfYouCan.view;

import java.awt.Color;

public interface ColorTheme {

     /**
      * Bestemmer fargen på brikken, fargene man kan velge mellom er:
      * Svart = '-'
      * Blå = 'a'
      * Blå = 'A'
      * Rød = 'b'
      * Rød = 'B'
      * Gul = 'Z'
      * Grønn = 'G'
      * 
      * @param
      * @return Color
      */

     public Color getCellColor(Character c);

     /**
      * Bestemmer fargen på på rammen.
      * 
      * @return Color
      */
     public Color getFrameColor();

     /**
      * Bestemmer fargen på på bakgrunn.
      * 
      * @return Color
      */
     public Color getBackgroundColor();

}
