package no.uib.inf101.sem2.catchMeIfYouCan.CatchMeController;

import java.awt.event.KeyEvent;

import no.uib.inf101.sem2.catchMeIfYouCan.model.GameState;
import no.uib.inf101.sem2.catchMeIfYouCan.view.CatchMeView;

import java.awt.event.ActionEvent;
import javax.swing.Timer;

// import javax.sound.sampled.AudioInputStream;
// import javax.sound.sampled.AudioSystem;
// import javax.sound.sampled.Clip;
// import java.io.File;

public class CatchMeController implements java.awt.event.KeyListener {

    ControllableCatchMeModel controllableCatchMeModel;
    CatchMeView catchMeView;
    GameState gameState;
    // Boolske flag, nyttig til å kontrollerer programflyt som i dette tilfellet
    // forhindres kontinuerlig bevegelse.
    // Hensikten er å forhindre "keyblocking".
    boolean player1MovingLeft = false;
    boolean player1MovingRight = false;
    boolean player1MovingUp = false;
    boolean player1MovingDown = false;
    boolean player2MovingLeft = false;
    boolean player2MovingRight = false;
    boolean player2MovingUp = false;
    boolean player2MovingDown = false;

    private final Timer timer;
                /**
     * Spillets kontroller som gir informasjon til modellen.
     * 
     * @param ControllableCatchMeModel
     * @param CatchMeView
     */
    public CatchMeController(ControllableCatchMeModel controllableCatchMeModel, CatchMeView catchMeView) {

        this.controllableCatchMeModel = controllableCatchMeModel;
        this.catchMeView = catchMeView;

        // this.timer = new Timer(controllableCatchMeModel.timer(), this::clockTick);
        // this.timer.start();

        this.timer = new Timer(controllableCatchMeModel.timerForVisualTimer(), this::clockTickCounter);
        this.timer.start();
        catchMeView.addKeyListener(this);
        gameState = GameState.ACTIVE_GAME;

        // Kode for musikk er til senere bruk.
        // if (gameState == GameState.ACTIVE_GAME){
        // try {
        // playSound("C:\\Users\\olav2\\OneDrive\\Skrivebord\\INF101\\sem2\\src\\main\\java\\no\\uib\\inf101\\sem2\\catchMeIfYouCan\\music\\WBA
        // Free Track - Hackers.wav");

        // } catch (Exception e) {System.out.println( "Wrong path");}

        // playSound("msrc/main/java/no/uib/inf101/sem2/catchMeIfYouCan/music/WBA Free
        // Track - Hackers.wav");

        // }

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (controllableCatchMeModel.getGameState() == GameState.ACTIVE_GAME) {

            if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                // Left arrow was pressed
                if (!player1MovingLeft) {
                    controllableCatchMeModel.moveCatchMeModel(1, 0, -1);
                    player1MovingLeft = true;
                }
            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                if (!player1MovingRight) {
                    // Right arrow was pressed
                    controllableCatchMeModel.moveCatchMeModel(1, 0, 1);
                    player1MovingRight = true;
                }
            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                if (!player1MovingDown) {
                    // Down arrow was pressed
                    controllableCatchMeModel.moveCatchMeModel(1, 1, 0);
                    player1MovingDown = true;
                }
            } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                if (!player1MovingUp) {
                    // Up arrow was pressed
                    controllableCatchMeModel.moveCatchMeModel(1, -1, 0);
                    player1MovingUp = true;
                }
            } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                // Spacebar was pressed
            } else if (e.getKeyCode() == KeyEvent.VK_A) {
                if (!player2MovingLeft) {

                    controllableCatchMeModel.moveCatchMeModel(2, 0, -1);
                    player2MovingLeft = true;
                }
            } else if (e.getKeyCode() == KeyEvent.VK_D) {
                if (!player2MovingRight) {
                    controllableCatchMeModel.moveCatchMeModel(2, 0, 1);
                    player2MovingRight = true;
                }
            } else if (e.getKeyCode() == KeyEvent.VK_W) {

                if (!player2MovingUp) {

                    controllableCatchMeModel.moveCatchMeModel(2, -1, 0);
                    player2MovingUp = true;
                }
            } else if (e.getKeyCode() == KeyEvent.VK_S) {
                if (!player2MovingDown) {
                    controllableCatchMeModel.moveCatchMeModel(2, 1, 0);
                    player2MovingDown = true;
                }
            }

            catchMeView.repaint();
        } else if (controllableCatchMeModel.getGameState() == GameState.ACTIVE_GAME) {

        } else
            catchMeView.repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (controllableCatchMeModel.getGameState() == GameState.ACTIVE_GAME) {
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                player1MovingLeft = false;
            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                player1MovingRight = false;
            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                player1MovingDown = false;
            } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                player1MovingUp = false;
            } else if (e.getKeyCode() == KeyEvent.VK_A) {
                player2MovingLeft = false;
            } else if (e.getKeyCode() == KeyEvent.VK_D) {
                player2MovingRight = false;
            } else if (e.getKeyCode() == KeyEvent.VK_W) {
                player2MovingUp = false;
            } else if (e.getKeyCode() == KeyEvent.VK_S) {
                player2MovingDown = false;
            }

        }

    }

    @Override
    public void keyTyped(KeyEvent arg0) {

    }

    public void clockTickCounter(ActionEvent countingDown) {
        if (gameState == GameState.ACTIVE_GAME) {
            controllableCatchMeModel.visualTimerAndChangingOfFightRun();

            delayControl();

            catchMeView.repaint();
        }
    }

    private void delayControl() {

        int delay = controllableCatchMeModel.timerForVisualTimer();

        timer.setDelay(delay);
        timer.setInitialDelay(delay);
    }

    // Kode skrevet med hjelp fra chat gpt.
    // public void playSound(String soundFilePath) {
    // try {
    // // Create an AudioInputStream from the given file path
    // File soundFile = new File(soundFilePath);
    // AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);

    // // Get a Clip object to play the audio
    // Clip clip = AudioSystem.getClip();

    // // Open the AudioInputStream and start playing the audio
    // clip.open(audioIn);
    // clip.start();
    // if (gameState == GameState.GAME_OVER){
    // clip.stop();
    // }
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    // }

}
