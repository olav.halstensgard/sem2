package no.uib.inf101.sem2.catchMeIfYouCan.view;

import no.uib.inf101.sem2.catchMeIfYouCan.model.GameState;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimensions;

public interface ViewableCatchMeModel { 

    /**
     * Returnerer en en GridDimension, contains number of rows and collumns of the
     * board.
     * 
     * @return GridDimension
     */
    GridDimensions getDimension();

    /**
     * Returnerer en en Iterable<GridCell<Character>>, som iterer over alle flisene
     * på brettet
     * 
     * @return Iterable<GridCell<Character>>
     */

    Iterable<GridCell<Character>> getTilesOnBoard();

    /**
     * Returnerer en en Iterable<GridCell<Character>>, som representere spiller1
     * 
     * @return Iterable<GridCell<Character>>
     */

    Iterable<GridCell<Character>> movingShape1();

    /**
     * Returnerer en en Iterable<GridCell<Character>>, som representere spiller2
     * 
     * @return Iterable<GridCell<Character>>
     */

    Iterable<GridCell<Character>> movingShape2();

    /**
     * Spiller 1 sin score.
     * 
     * @return int
     */

    int getScorePlayer1();

    /**
     * Spiller 2 sin score.
     * 
     * @return int
     */

    int getScorePlayer2();

    /**
     * Hvor mye tid som er igjen.
     * 
     * @return int
     */
    int timeLeft();

    /**
     * Instruks høyre.
     * 
     * @return String
     */
    String fightRunPlayer1();

    /**
     * Instruks venstre
     * 
     * @return String
     */

    String fightRunPlayer2();

    /**
     * Skiller mellom ACTIVE_GAME og GAME_OVER. Når spillet får status GAME_OVER så
     * avsluttes det.
     * 
     * @return GameStat
     */
    GameState getGameState();

}
