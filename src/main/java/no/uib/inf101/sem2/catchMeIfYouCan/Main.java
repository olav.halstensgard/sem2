package no.uib.inf101.sem2.catchMeIfYouCan;

import javax.swing.JFrame;

import no.uib.inf101.sem2.catchMeIfYouCan.CatchMeController.CatchMeController;
import no.uib.inf101.sem2.catchMeIfYouCan.model.CatchMeBoard;
import no.uib.inf101.sem2.catchMeIfYouCan.model.CatchMeModel;
import no.uib.inf101.sem2.catchMeIfYouCan.model.playerUnit.PlayerState;
import no.uib.inf101.sem2.catchMeIfYouCan.view.CatchMeView;
import no.uib.inf101.sem2.catchMeIfYouCan.model.maps.Maps;

public class Main {
  public static void main(String[] args) {

    Maps maps = new Maps();
    CatchMeBoard board = maps.map1();

    PlayerState player1 = new PlayerState(1);
    PlayerState player2 = new PlayerState(2);

    CatchMeModel model = new CatchMeModel(board, player1, player2);

    CatchMeView view = new CatchMeView(model);

    CatchMeController controller = new CatchMeController(model, view);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("_Catch me if you can_");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}
