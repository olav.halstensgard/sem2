package no.uib.inf101.sem2.catchMeIfYouCan.model.playerUnit;

public class PlayerState implements CreatePlayer {

    private int playerNumber;

    /**
     * Spiller 1 må være int 1 og spiller 2 må være int 2
     * 
     * @param int number
     */
    public PlayerState(int number) {

        playerNumber = number;

    }

    @Override
    public PlayerUnit createPlayers() {

        PlayerUnit playerUnit = PlayerUnit.newPlayerUnit(getShape());

        return playerUnit;
    }

    private char getShape() {

        if (playerNumber == 1) {
            char shapeA = 'a';

            return shapeA;
        }
        if (playerNumber == 2) {

            char shapeB = 'b';

            return shapeB;
        }

        else {
            throw new IllegalArgumentException("There can only be two players");
        }
    }

}
