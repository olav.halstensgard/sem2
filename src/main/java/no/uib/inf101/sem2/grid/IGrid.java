package no.uib.inf101.sem2.grid;

public interface IGrid<E> extends GridDimensions, Iterable<GridCell<E>> {
  //Dette er logikk som ble brukt i tetris som jeg har gjennbrukt.
  /**
   * Setter verdien til en posisjon i gridet. Samt et følgende kall til
   * {@link #get}
   * med en lik posisjon som argument så vil return verdien være den som ble satt
   * Metoden vil overskrive den tidligere verdien til cellen.
   * 
   * 
   * @param pos   posisjonen hvor det skal settes en ny verdi
   * @param value ny verdi
   * @throws IndexOutOfBoundsException dersom posisjonen ikke eksistere på gridet.
   */
  void set(CellPosition pos, E value);

  /**
   * Henter den nåværende verdien til posisjonen
   * 
   * @param pos Hvilke posisjon som hentes
   * @return Hvilke verdi som er lagret på git posisjon
   * @throws IndexOutOfBoundsException dersom posisjonen ikke eksistere på gridet.
   */
  E get(CellPosition pos);

  /**
   * Gir beskjed dersom posisjonen er utenfor grensene til gridet.
   * 
   * @param pos posisjoen som sjekkes
   * @return Boolsk verdi, true dersom er på gridet, false dersom ikke.
   */
  boolean positionIsOnGrid(CellPosition pos);
}
