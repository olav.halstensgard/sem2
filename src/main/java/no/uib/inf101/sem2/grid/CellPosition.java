package no.uib.inf101.sem2.grid;

/**
 * Record er en immutable klasse
 * 
 * @param row the row of the cell
 * @param col the column of the cell
 */

public record CellPosition(int row, int col) {

}
