package no.uib.inf101.sem2.grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Grid<E> implements IGrid<E> {

    int numberOfRows;
    int numberOfCollumns;

    private List<List<E>> grid;

    /**
     * Lager en to dimensjonal liste som tilsvarer et kordinatsystem. defaultValue
     * må ikke tilegnes noen verdi, men kan spesifiseres etter behov
     * 
     * @param numberOfRows
     * @param numberOfCollumns
     * 
     */
    public Grid(int numberOfRows, int numberOfCollumns) {

        this(numberOfRows, numberOfCollumns, null);

    }

    /**
     * Lager en to dimensjonal liste som tilsvarer et kordinatsystem. defaultValue
     * må ikke tilegnes noen verdi, men kan spesifiseres etter behov
     * 
     * @param numberOfRows
     * @param numberOfCollumns
     * @param defaultValue
     */
    public Grid(int numberOfRows, int numberOfCollumns, E defaultValue) {
        this.numberOfRows = numberOfRows;
        this.numberOfCollumns = numberOfCollumns;

        this.grid = new ArrayList<List<E>>();

        for (int row = 0; row < numberOfRows; row++) {
            List<E> eRow = new ArrayList<>();
            for (int column = 0; column < numberOfCollumns; column++) {
                eRow.add(defaultValue);

            }

            grid.add(eRow);
        }

    }

    @Override
    public int rows() {
        return numberOfRows;
    }

    @Override
    public int cols() {
        return numberOfCollumns;
    }

    /**
     * Her returneres en iterator som itererer over alle cellene i gridet.
     * Iteratoren itererer over verdiene CellPosition og value,
     * 
     *
     * @return en iterator som itererer over cellene i grid'et.
     */
    @Override
    public Iterator<GridCell<E>> iterator() {
        List<GridCell<E>> positionAndValue = new ArrayList<>();
        for (int r = 0; r < numberOfRows; r++) {
            for (int c = 0; c < numberOfCollumns; c++) {
                CellPosition position = new CellPosition(r, c);
                E value = get(position);
                GridCell<E> gridCell = new GridCell<>(position, value);
                positionAndValue.add(gridCell);

            }

        }

        return positionAndValue.iterator();

    }

    @Override
    public void set(CellPosition pos, E value) {
        int row = pos.row();
        int col = pos.col();
        grid.get(row).set(col, value);
    }

    @Override
    public E get(CellPosition pos) {
        int rowIndex = pos.row();
        int colIndex = pos.col();

        List<E> row = this.grid.get(rowIndex);
        E result = row.get(colIndex);

        return result;
    }

    @Override
    public boolean positionIsOnGrid(CellPosition pos) {

        int xPos = pos.row();
        int yPos = pos.col();

        return numberOfRows > xPos && numberOfCollumns > yPos && 0 <= xPos && 0 <= yPos;

    }

}
