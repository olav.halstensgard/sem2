package no.uib.inf101.sem2.grid;

public interface GridDimensions {

      /** Antall rader i gridet*/
  int rows();

  /** Antall kolonner i gridet*/
  int cols();
    
}
