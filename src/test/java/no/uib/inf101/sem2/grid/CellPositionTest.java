package no.uib.inf101.sem2.grid;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Objects;

import org.junit.jupiter.api.Test;

//Denne testen er tatt fra tetris
public class CellPositionTest {

  @Test
  void sanityTest() {
    CellPosition cp = new CellPosition(3, 2);
    assertEquals(3, cp.row());
    assertEquals(2, cp.col());
  }

  @Test
  void coordinateEqualityTest() {
    CellPosition a = new CellPosition(4, 5);
    CellPosition b = new CellPosition(4, 5);

    assertFalse(a == b);
    assertTrue(a.equals(b));
    assertTrue(b.equals(a));
    assertTrue(Objects.equals(a, b));
  }

  @Test
  void coordinateInequalityTest() {
    CellPosition a = new CellPosition(8, 9);
    CellPosition b = new CellPosition(9, 8);

    assertFalse(a == b);
    assertFalse(a.equals(b));
    assertFalse(b.equals(a));
    assertFalse(Objects.equals(a, b));
  }

  @Test
  void coordinateHashcodeTest() {
    CellPosition a = new CellPosition(2, 3);
    CellPosition b = new CellPosition(2, 3);
    assertTrue(a.hashCode() == b.hashCode());

    CellPosition c = new CellPosition(85, 85);
    CellPosition d = new CellPosition(85, 85);
    assertTrue(c.hashCode() == d.hashCode());
  }

}
