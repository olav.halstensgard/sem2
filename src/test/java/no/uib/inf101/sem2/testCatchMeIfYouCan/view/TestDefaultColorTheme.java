package no.uib.inf101.sem2.testCatchMeIfYouCan.view;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.awt.Color;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.catchMeIfYouCan.view.ColorTheme;
import no.uib.inf101.sem2.catchMeIfYouCan.view.DefaultColorTheme;

public class TestDefaultColorTheme {
  @Test
  public void sanityTestDefaultColorTheme() {
    ColorTheme colors = new DefaultColorTheme();
    assertEquals(new Color(245, 245, 220), colors.getBackgroundColor());
    assertEquals(new Color(0, 0, 0, 0), colors.getFrameColor());
    assertEquals(Color.BLACK, colors.getCellColor('-'));
    assertEquals(Color.BLUE, colors.getCellColor('a'));
    assertEquals(Color.RED, colors.getCellColor('b'));
    assertEquals(Color.YELLOW, colors.getCellColor('Z'));
    assertThrows(IllegalArgumentException.class, () -> colors.getCellColor('\n'));
  }

}
