package no.uib.inf101.sem2.testCatchMeIfYouCan.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.catchMeIfYouCan.model.CatchMeBoard;
import no.uib.inf101.sem2.catchMeIfYouCan.model.CatchMeModel;
import no.uib.inf101.sem2.catchMeIfYouCan.model.playerUnit.CreatePlayer;
import no.uib.inf101.sem2.grid.CellPosition;

import no.uib.inf101.sem2.testCatchMeIfYouCan.model.playerUnit.IntendedCatchMeModel;

public class TestCatchMeModel {

    @Test
    public void testLegalityOfMove() {
        CatchMeBoard board = new CatchMeBoard(4, 5);
        CreatePlayer player1 = new IntendedCatchMeModel("a");
        CreatePlayer player2 = new IntendedCatchMeModel("b");

        board.set(new CellPosition(3, 0), 'Z');
        board.set(new CellPosition(2, 0), 'Z');
        board.set(new CellPosition(3, 1), 'Z');
        board.set(new CellPosition(2, 1), 'Z');

        CatchMeModel model = new CatchMeModel(board, player1, player2);
        // Test mot vegg
        assertTrue(model.moveCatchMeModel(1, 1, 1));
        assertFalse(model.moveCatchMeModel(1, 3, 0));
        assertTrue(model.moveCatchMeModel(2, -1, 0));
        assertFalse(model.moveCatchMeModel(2, 3, 1));

        // Utenfor brettet
        assertFalse(model.moveCatchMeModel(1, -8, -8));
        assertFalse(model.moveCatchMeModel(2, -8, -8));

    }

}
