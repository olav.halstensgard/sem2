package no.uib.inf101.sem2.testCatchMeIfYouCan.model.playerUnit;

import no.uib.inf101.sem2.catchMeIfYouCan.model.playerUnit.CreatePlayer;
import no.uib.inf101.sem2.catchMeIfYouCan.model.playerUnit.PlayerUnit;

public class IntendedCatchMeModel implements CreatePlayer {

    String s;
    int counter;

    public IntendedCatchMeModel(String s) {

        this.s = s;
        this.counter = 0;

    }

    @Override
    public PlayerUnit createPlayers() {
        char c = s.charAt(counter);

        counter++;
        if (counter == s.length()) {
            counter = 0;
        }

        return PlayerUnit.newPlayerUnit(c);

    }

}
