package no.uib.inf101.sem2.testCatchMeIfYouCan.model.playerUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.catchMeIfYouCan.model.CatchMeBoard;
import no.uib.inf101.sem2.catchMeIfYouCan.model.playerUnit.PlayerUnit;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;

public class TestPlayerUnit {

  @Test
  public void testNoDuplicate() {
    PlayerUnit t1 = PlayerUnit.newPlayerUnit('a');
    PlayerUnit t2 = PlayerUnit.newPlayerUnit('b');
    PlayerUnit t3 = PlayerUnit.newPlayerUnit('a').nextMove(2, 0);
    PlayerUnit s1 = PlayerUnit.newPlayerUnit('b');
    PlayerUnit s2 = PlayerUnit.newPlayerUnit('b').nextMove(1, 0);

    assertNotEquals(t1, t2);
    assertNotEquals(s1, s2);
    assertNotEquals(t1.hashCode(), t2.hashCode());
    assertNotEquals(s1.hashCode(), s2.hashCode());
    assertNotEquals(t1, t3);
    assertNotEquals(t1, s1);
  }

  @Test
  public void correctPositionOfPlayers() {

    PlayerUnit playerUnit1 = PlayerUnit.newPlayerUnit('a');
    playerUnit1 = playerUnit1.nextMove(5, 50);
    PlayerUnit playerUnit2 = PlayerUnit.newPlayerUnit('b');
    playerUnit2 = playerUnit2.nextMove(10, 5);

    // Collect which objects are iterated through
    List<GridCell<Character>> objs = new ArrayList<>();
    for (GridCell<Character> gc : playerUnit1) {
      objs.add(gc);

    }

    List<GridCell<Character>> objs2 = new ArrayList<>();
    for (GridCell<Character> gc : playerUnit2) {
      objs2.add(gc);

      // Check that we got the expected GridCell objects
      assertEquals(1, objs.size());
      assertEquals(1, objs2.size());
      assertTrue(objs.contains(new GridCell<>(new CellPosition(6, 51), 'a')));
      assertTrue(objs2.contains(new GridCell<>(new CellPosition(11, 6), 'b')));

    }

  }

  @Test
  public void moveAsIntended() {

    PlayerUnit playerUnit1 = PlayerUnit.newPlayerUnit('a');
    PlayerUnit playerUnit2 = PlayerUnit.newPlayerUnit('b');
    playerUnit1 = playerUnit1.nextMove(1, 1);
    playerUnit2 = playerUnit2.nextMove(2, -1);

    List<GridCell<Character>> objs = new ArrayList<>();
    for (GridCell<Character> gc : playerUnit1) {
      objs.add(gc);
    }
    for (GridCell<Character> gc : playerUnit2) {
      objs.add(gc);
    }

    assertEquals(2, objs.size());
    assertTrue(objs.contains(new GridCell<>(new CellPosition(2, 2), 'a')));
    // Sjekker at riktig verdi settes.
    assertTrue(objs.contains(new GridCell<>(new CellPosition(3, 0), 'b')));

    assertFalse(objs.contains(new GridCell<>(new CellPosition(3, 0), 'a')));

  }

  @Test
  public void testPlayerSpawnIfOdd() {
    CatchMeBoard board = new CatchMeBoard(45, 45);
    PlayerUnit player1 = PlayerUnit.newPlayerUnit('a');
    PlayerUnit player2 = PlayerUnit.newPlayerUnit('b');

    player1 = player1.playerSpawn(1, board);
    player2 = player2.playerSpawn(2, board);

    List<GridCell<Character>> objs = new ArrayList<>();
    for (GridCell<Character> gc : player1) {
      objs.add(gc);
    }

    for (GridCell<Character> gc : player2) {
      objs.add(gc);
    }
    assertEquals(2, objs.size());
    assertTrue(objs.contains(new GridCell<>(new CellPosition(22, 43), 'a')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(22, 1), 'b')));

  }

  @Test
  public void testPlayerSpawnIfEven() {
    CatchMeBoard board = new CatchMeBoard(10, 10);
    PlayerUnit player1 = PlayerUnit.newPlayerUnit('a');
    PlayerUnit player2 = PlayerUnit.newPlayerUnit('b');

    player1 = player1.playerSpawn(1, board);
    player2 = player2.playerSpawn(2, board);

    List<GridCell<Character>> objs = new ArrayList<>();
    for (GridCell<Character> gc : player1) {
      objs.add(gc);
    }

    for (GridCell<Character> gc : player2) {
      objs.add(gc);
    }
    assertEquals(2, objs.size());
    assertTrue(objs.contains(new GridCell<>(new CellPosition(4, 8), 'a')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(4, 1), 'b')));

  }

}