package no.uib.inf101.sem2.testCatchMeIfYouCan.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.catchMeIfYouCan.model.CatchMeBoard;
import no.uib.inf101.sem2.grid.CellPosition;

public class TestCatchMeBoard {
  // Test metode tatt fra tetris. De hard kodede kartene fungerer også som test på
  // at set() metoden fungere som den skal. Dette får man bekreftet ved å starte
  // spillet.
  @Test
  public void testPrettyString() {
    CatchMeBoard board = new CatchMeBoard(3, 4);
    board.set(new CellPosition(0, 0), 'g');
    board.set(new CellPosition(0, 3), 'y');
    board.set(new CellPosition(2, 0), 'r');
    board.set(new CellPosition(2, 3), 'b');
    String expected = String.join("\n", new String[] {
        "g--y",
        "----",
        "r--b"
    });
    assertEquals(expected, board.prettyString());
  }
}