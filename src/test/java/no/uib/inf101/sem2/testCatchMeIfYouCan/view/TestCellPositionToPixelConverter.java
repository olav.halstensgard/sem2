package no.uib.inf101.sem2.testCatchMeIfYouCan.view;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.catchMeIfYouCan.model.CatchMeBoard;
import no.uib.inf101.sem2.catchMeIfYouCan.view.CellPositionToPixelConverter;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimensions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.geom.Rectangle2D;

public class TestCellPositionToPixelConverter {

    @Test
    public void sanityTest() {
        GridDimensions gd = new CatchMeBoard(3, 4);
        CellPositionToPixelConverter converter = new CellPositionToPixelConverter(
                new Rectangle2D.Double(29, 29, 340, 240), gd, 30);
        Rectangle2D expected = new Rectangle2D.Double(214, 129, 47.5, 40);
        assertEquals(expected, converter.getBoundsForCell(new CellPosition(1, 2)));

        Rectangle2D expected2 = new Rectangle2D.Double(369, 199, 47.5, 40);
        assertEquals(expected2, converter.getBoundsForCell(new CellPosition(2, 4)));

    }
}
